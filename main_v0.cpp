////////////////////////////////////////////////////////////////////////
// OOP Tutorial: Simple C++ OO program to simulate a simple Dice Game
////////////////////////////////////////////////////////////////////////

//--------include libraries
#include <ctime>	//for time used in random number routines
#include <iostream>	//for cin >> and cout <<
#include <string>	//for string routines

using namespace std;

//--------RandomNumberGenerator class
class RandomNumberGenerator {
	public:
		RandomNumberGenerator();
		~RandomNumberGenerator();
		static int getRandomValue(int);
	private:
		static void seed();
};
RandomNumberGenerator::RandomNumberGenerator()
{
//	cout << "\n___RandomNumberGenerator default constructor called\n";
	seed();	//reset the random number generator from current system time
}
RandomNumberGenerator::~RandomNumberGenerator()
{
//	cout << "\n___RandomNumberGenerator destructor called\n";
}
void RandomNumberGenerator::seed() { //static
	srand(static_cast<unsigned int>(time(0)));
}
int RandomNumberGenerator::getRandomValue(int max) { //static
	return (rand() % max) + 1; //produce a random number in range [1..max]
}
//--------end of RandomNumberGenerator class

//--------Dice class
class Dice {
	public:
		Dice();
		~Dice();
		void setRNG(RandomNumberGenerator* prng);
		int getFace() const;
		void roll();
	private:
		int face_;	//number on dice
		RandomNumberGenerator* p_rng_; 	//address of a RandomNumberGenerator
};
Dice::Dice() : face_(0), p_rng_(nullptr)
{
//	cout << "\n___Dice default constructor called\n";
}
Dice::~Dice()
{
//	cout << "\n___Dice destructor called\n";
}
int Dice::getFace() const {
	return face_;	//get value of dice face
}
void Dice::setRNG(RandomNumberGenerator* prng) {
	p_rng_ = prng;	//store address of the RandomNumberGenerator
}
void Dice::roll() {
	face_ = (*p_rng_).getRandomValue(6); 	//roll dice
}
//--------end of Dice class

//--------Score class
class Score {
	public:
		Score();
		Score(const Score& s);
		~Score();
		int getAmount() const;
		void updateAmount(int);
	private:
		int amount_;
};
Score::Score()
: amount_(0) 
{
//	cout << "\n___Score default constructor called\n";
}
Score::Score(const Score& s) 
: amount_(s.amount_)
{
//	cout << "\n___Score copy constructor called\n";
}
Score::~Score() {
//	cout << "\n___Score destructor called\n";
}
int Score::getAmount() const {
	return amount_;
}
void Score::updateAmount(int value) {
//increment when value>0, decrement otherwise
	amount_ += value;
}
//--------end of Score class

//--------Player class
class Player {
	public:
		Player();
		~Player();
		string getName() const;
		Score getScore() const;
		void readInName();
		void updateScore(int);
		static int readInNumberOfGoes();
	private:
		string name_;
		Score score_;
};
Player::Player() 
: name_(), score_() 
{
//	cout << "\n___Player default constructor called\n";
}
Player::~Player() {
//	cout << "\n___Player destructor called\n";
}
Score Player::getScore() const {
	return score_;
}
string Player::getName() const {
	return name_;
}
void Player::updateScore(int value) {
	//TO BE IMPLEMENTED FOLLOWING SEQUENCE DIAGRAM GIVEN	
	cout << "\nvoid Player::updateScore(int value) NOT IMPLEMENTED YET!";
}
void Player::readInName() {
//ask the user for his/her name
	cout << "\nEnter your name? ";
	cin >> name_;
}
int Player::readInNumberOfGoes() { //static
//ask the user for the number of dice throws
	int num;
	cout << "\nHow many go do you want? ";
	cin >> num;
	return num;
}

//--------end of Player class

//--------Game class
class Game {
	public:
		Game();
		~Game();
		Player getPlayer() const;
		void setUp(Player* pplayer, RandomNumberGenerator* prng);
		void displayData() const;
		void run();
	private:
		Player* p_player_;
		Dice firstDice_, secondDice_;
		int numberOfGoes_;
		void rollDices();
};
Game::Game() 
: p_player_(nullptr), firstDice_(), secondDice_(), numberOfGoes_(0) 
{
//	cout << "\n___Game default constructor called";
}
Game::~Game() {
//	cout << "\n___Game destructor called";
}
Player Game::getPlayer() const {
	return *p_player_;	//returns the player
}
void Game::setUp(Player* pplayer,  RandomNumberGenerator* prng) {
	p_player_ = pplayer;
	firstDice_.setRNG(prng);
	secondDice_.setRNG(prng);
	p_player_->readInName();
	numberOfGoes_ = p_player_->readInNumberOfGoes();
}
void Game::displayData() const {
	cout << "\nPlayer is: " << p_player_->getName();
	cout << "\nScore is: " << p_player_->getScore().getAmount() << endl;
}
void Game::run() {
	for(int i(1); i <= numberOfGoes_; i++)
	{
		//TO BE IMPLEMENTED FOLLOWING SEQUENCE DIAGRAM GIVEN
		cout << "\nvoid Game::run() NOT IMPLEMENTED YET!";
	}
}
void Game::rollDices() {
	firstDice_.roll();
	secondDice_.roll();
}
//--------end of Game class

//---------------------------------------------------------------------------
//with two dices
int main() {
	RandomNumberGenerator rng;
	Player player;
	Game twoDiceGame;

	twoDiceGame.setUp(&player, &rng);
	cout << "\n________________________";
	cout << "\nGame starting...";
	twoDiceGame.displayData();
	cout << "\n________________________";
	twoDiceGame.run();
	cout << "\n________________________";
	cout << "\nGame ended...";
	twoDiceGame.displayData();
	cout << "\n________________________";

	cout << endl << endl;
	system("pause");
	return 0;
}


